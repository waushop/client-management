import React, { Component } from 'react';
import DataTable from "@bit/adeoy.utils.data-table";
import { Link } from 'react-router-dom';

const columns = [
  { title: "Name", data: "name" },
  { title: "Address", data: "address" },
  { title: "City", data: "city" },
  { title: "Country Code", data: "countryCode" },
  { title: "Loan Amount", data: "loanAmount" },
];

const click = (row) => {
  //console.log(row);
  window.location.href = "/edit-client";
};

class ClientList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      clients: []
    }
  }

  async componentDidMount() {
    const response = await fetch('/api/clients');
    const body = await response.json();
    this.setState({ clients: body, isLoading: false });
  }

  render() {
    const { clients, isLoading } = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    return (
      <>
        <h1 className="mt-5 mb-4 text-center">Clients</h1>
        <DataTable
          data={clients}
          columns={columns}
          striped={false}
          hover={true}
          responsive={true}
          onClickRow={click}
        />
        <Link to="/add-client" className="btn btn-success btn-block mt-3">Add new client</Link>
      </>
    );
  }
}

export default ClientList;