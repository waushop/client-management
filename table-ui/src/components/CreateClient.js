import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class CreateClient extends Component {

    constructor(props) {
        super(props)

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeAddress = this.onChangeAddress.bind(this);
        this.onChangeCity = this.onChangeCity.bind(this);
        this.onChangeCountryCode = this.onChangeCountryCode.bind(this);
        this.onChangeLoanAmount = this.onChangeLoanAmount.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            address: '',
            city: '',
            countryCode: '',
            loanAmount: ''
        }
    }

    onChangeName(e) {
        this.setState({ name: e.target.value })
    }

    onChangeAddress(e) {
        this.setState({ address: e.target.value })
    }
    onChangeCity(e) {
        this.setState({ city: e.target.value })
    }
    onChangeCountryCode(e) {
        this.setState({ countryCode: e.target.value })
    }
    onChangeLoanAmount(e) {
        this.setState({ loanAmount: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        const clientObject = {
            name: this.state.name,
            address: this.state.address,
            city: this.state.city,
            countryCode: this.state.countryCode,
            loanAmount: this.state.loanAmount
        };

        axios.post('/api/clients', clientObject)
            .then((res) => {
                console.log(res.data)
            }).catch((error) => {
                console.log(error)
            });

        this.setState({ name: '', address: '', city: '', countryCode: '', loanAmount: '' })

        window.location.href = "/";

    }

    render() {
        return (
            <div className="wrapper">
                <h1 className="mt-5 mb-4 text-center">Add new client</h1>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" value={this.state.name} onChange={this.onChangeName} className="form-control" required />
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Address</label>
                                <input type="text" value={this.state.address} onChange={this.onChangeAddress} className="form-control" required />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>City</label>
                                <input type="text" value={this.state.city} onChange={this.onChangeCity} className="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Country code</label>
                                <input type="text" value={this.state.countryCode} onChange={this.onChangeCountryCode} className="form-control" required />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Loan amount</label>
                                <input type="text" value={this.state.loanAmount} onChange={this.onChangeLoanAmount} className="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Add client" className="btn btn-success btn-block mt-3" />
                    </div>
                </form>
                <Link to="/" className="btn btn-dark btn-block">Back to clients list</Link>
            </div>
        )
    }
}