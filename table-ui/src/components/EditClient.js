import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class CreateClient extends Component {
   
    render() {
        return (
            <div className="wrapper">
                <h1 className="mt-5 mb-4 text-center">Edit client</h1>
                <form>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control" required />
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Address</label>
                                <input type="text" className="form-control" required />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>City</label>
                                <input type="text" className="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Country code</label>
                                <input type="text" className="form-control" required />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Loan amount</label>
                                <input type="text" className="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Save changes" className="btn btn-success btn-block mt-3" />
                        <Link to="#" className="btn btn-danger btn-block mt-3">Delete client</Link>
                    </div>
                </form>
                <Link to="/" className="btn btn-dark btn-block">Back to clients list</Link>
            </div>
        )
    }
}