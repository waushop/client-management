import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import ClientList from "./ClientList";
import CreateClient from "./CreateClient";
import EditClient from "./EditClient";

const AppRouter = () => {
    return(
        <div style={style}>
            <Router>
                    <Switch>
                        <Route path="/" exact component={ClientList} />
                        <Route path="/add-client" component={CreateClient} />
                        <Route path="/edit-client" component={EditClient} />
                    </Switch>
            </Router>
        </div>
    )
}

const style={
    marginTop:'20px'
}

export default AppRouter;