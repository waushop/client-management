package com.datatable.table.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datatable.table.service.ClientService;
import com.datatable.table.entity.Client;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ClientController {

    @Autowired
	private ClientService clientService;
	
	@GetMapping("/clients")
	public List<Client> get() {
		return clientService.get();
	}
	
	@PostMapping("/clients")
	public Client save(@RequestBody final Client client) {
		clientService.save(client);
		return client;
	}
	
	@GetMapping("/clients/{id}")
	public Client get(@PathVariable final int id) {
		return clientService.get(id);
	}
	
	@DeleteMapping("/clients/{id}")
	public String delete(@PathVariable final int id) {
		
		clientService.delete(id);
		
		return "Client removed with id "+id;
		
	}

	@PutMapping("/clients/{id}")
	public Client update(@RequestBody final Client client) {
		clientService.save(client);
		return client;
	}
    
}
