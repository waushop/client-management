package com.datatable.table.service;

import java.util.List;

import com.datatable.table.entity.Client;

public interface ClientService {
	

	List<Client> get();
	
	Client get(int id);
	
	void save(Client client);

	void update(Client client);
	
	void delete(int id);
	

}