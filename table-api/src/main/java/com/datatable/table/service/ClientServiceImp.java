package com.datatable.table.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datatable.table.dao.ClientDAO;
import com.datatable.table.entity.Client;

@Service
public class ClientServiceImp implements ClientService {
	
	@Autowired
	private ClientDAO clientDao;

	@Transactional
	@Override
	public List<Client> get() {
		return clientDao.get();
	}

	@Transactional
	@Override
	public Client get(int id) {
		return clientDao.get(id);
	}

	@Transactional
	@Override
	public void save(Client alarm) {
		clientDao.save(alarm);
		
	}

	@Transactional
	@Override
	public void update(Client client) {
		clientDao.save(client);
	}

	@Transactional
	@Override
	public void delete(int id) {
		clientDao.delete(id);
		
	}

}