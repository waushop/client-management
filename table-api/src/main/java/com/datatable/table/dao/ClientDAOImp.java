package com.datatable.table.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.datatable.table.entity.Client;

@Repository
public class ClientDAOImp implements ClientDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Client> get() {

		Session currSession = entityManager.unwrap(Session.class);
		Query<Client> query = currSession.createQuery("from Client", Client.class);
		List<Client> list = query.getResultList();
		return list;

	}

	@Override
	public Client get(int id) {
		Session currSession = entityManager.unwrap(Session.class);
		Client cl = currSession.get(Client.class, id);
		return cl;
	}

	@Override
	public void save(Client client) {
		
		Session currSession = entityManager.unwrap(Session.class);
		currSession.saveOrUpdate(client);

	}

	@Override
	public void delete(int id) {
		Session currSession = entityManager.unwrap(Session.class);
		Client cl = currSession.get(Client.class, id);
		currSession.delete(cl);

	}

}