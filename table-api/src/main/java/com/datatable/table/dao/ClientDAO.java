package com.datatable.table.dao;

import java.util.List;

import com.datatable.table.entity.Client;

public interface ClientDAO {
	
	List<Client> get();
	
	Client get(int id);
	
	void save(Client client);
	
	void delete(int id);
	

}